Rails.application.routes.draw do
  
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'messages#index' 
  #temporary root path for now 
 
  resources :users, only:[:new, :create]
  resources :messages


end
