class GamesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "games"
    
    card = ApplicationController.render(partial: "users/user", locals: {
      user: current_user
    })
    
    current_user.update_attributes(online: true)

    ActionCable.server.broadcast('games', user: current_user, card: card, subscribe: true)
  
  end

  def unsubscribed
  
    current_user.update_attributes(online: false)

    ActionCable.server.broadcast('games', user: current_user, subscribe: false)
  end

  def add
    ActionCable.server.broadcast('games', user: current_user, add: true)
  end

  def winner(data)

    if Game.first.on == true

      puts "ici"

      user = User.find_by(id: data["id"])

      html = ApplicationController.render(partial: "users/winner", locals: {
        winner: user
      })
      ActionCable.server.broadcast('games', winner: html, end: true)

      game.update_attributes(on: false)

    elsif Game.first.on == false

      puts "toooo late"

    end
  end 

  def timer
    ActionCable.server.broadcast('games', timer: true)
  end

  def start
    ActionCable.server.broadcast('games', game: true)

    Game.first.update_attributes(on: true)

  end

  def end
    ActionCable.server.broadcast('games', game: false)
  end
end
