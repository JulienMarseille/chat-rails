class MessagesController < ApplicationController

    def index

        if current_user && user_signed_in?
            @users = User.where(online: true)
        else

            return redirect_to new_user_session_path

        end

    end

end
