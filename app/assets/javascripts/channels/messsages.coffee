App.messsages = App.cable.subscriptions.create "GamesChannel",
  connected: (data) ->

    $(document).on 'click', "#play",  (event) =>

      @startTimer()

    $(document).on 'keyup', (event) =>

      if event.keyCode is 32 && $("#go").attr("id") == "go"

        @addOneBar()

    # Called when the subscription is ready for use on the server

  received: (data) ->
  
    if data.subscribe is true

      if document.getElementById(data.user.id) is null

        $("#game_space").append(data.card)

    if data.add is true

      bar = $( "<div class='bar'" + data.user.id + "></div>" )

      document.getElementById(data.user.id).children[0].append( bar[0] )

      if document.getElementById(data.user.id).children[0].childElementCount > 30

        @winner(data)
        

    if data.subscribe is false

      document.getElementById(data.user.id).remove()
    
    if data.end is true

      $("#winner-list").append(data.winner)

      $(".bar").remove()

      $("#go").remove()

      $('#timer').html("<div id='play'>Jouer</div>")
    
      while $(".winner").length >= 2

        $("#winner-list div:last-child").remove()


    if data.timer is true

      $(".bar").remove()

      $(".winner").remove()

      count = 3

      timer = setInterval () =>

        if count == 0

          $('#timer').html("")

          $('#timer').append( "<div id='go'>GOOOOOOOO</div>" )

          clearInterval(timer)

          count = 3

        else

          $('#timer').html("<div id='count'>" + count + "</div>")

          count--

      , 1000



  addOneBar: () ->

    @perform 'add'

    
    # Called when there's incoming data on the websocket for this channel

  startTimer: () ->

    @perform 'timer'

  startGame: () ->

    @perform 'start'

  winner: (data) ->

    @perform 'winner', { id: data.user.id }



    



